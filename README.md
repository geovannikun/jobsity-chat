# README #

Project for jobsity nodejs chat bot challenge.  
This project uses a monorepo like structure.  

As a challenge I took free will to assume some stuffs like sign up with username and password only.  
In real world I would solve any doubts with the client to delivery the expected product.  

## Setup ###

This project use `yarn`.  
This project also include a `.nvmrc` file, so before proceed run `nvm install` and `nvm use`.  
To install all dependencies just run `yarn` at root folder.  
  
If you prefer to use `npm` you will have to access each subfolder and run `npm install`  

## Run the project ###

To run the project just run `docker-compose up` at root folder.  

### __Run Chat Server__

For some reason trying to login or create a user crash the container, so you will also need to run the chat server manually.  

- Run `cd chat-server`  
- Copy `.env.example` file and paste as `.env`  
- Run `yarn dev`  

You can access the project at `http://localhost:3000`  

## Problems you may find ###

Some times send messages at the chat don't work. This is because you probably deleted you database or logged with the same user at another browser, so the logged user token is not valid anymore.  
To solve this just remove the key `token` from Local Storage  

## TODO ###

- [ ] Better error handing at frontend
