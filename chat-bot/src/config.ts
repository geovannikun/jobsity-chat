import dotenv from 'dotenv'

const environment = process.env.NODE_ENV || 'development'

if (environment === 'development') {
  dotenv.config({
    path: require('path').resolve(process.cwd(), `.env`)
  })
}

export const Config = {
  environment,
  rabbitmqUsername: process.env.RABBITMQ_USERNAME,
  rabbitmqPassword: process.env.RABBITMQ_PASSWORD,
  rabbitmqHost: process.env.RABBITMQ_HOST,
  rabbitmqPort: process.env.RABBITMQ_PORT,
}
