import https from 'https'
import csv from 'csv-parser'

interface IMarketStock {
  Symbol: string
  Date: string
  Time: string
  Open: string
  High: string
  Low: string
  Close: string
  Volume: string
}

class StockServiceImpl {
  private fetchCSVLastRow = <T>(url: string) => new Promise<T>((resolve, reject) => {
    https.get(url, (resp) => {
      let result: T
      resp
        .pipe(csv())
        .on('data', (data: T) => {
          result = data
        })
        .on('end', () => {
          resolve(result)
        })
        .on('error', (error) => {
          reject(error)
        })
    })
  })

  async getByStockQuote(stockCode: string) {
    const stock = await this.fetchCSVLastRow<IMarketStock>(`https://stooq.com/q/l/?s=${stockCode}&f=sd2t2ohlcv&h&e=csv`)
    return stock.Close === 'N/D' ? undefined : stock.Close
  }
}

export const StockService = new StockServiceImpl()
