import { Config } from "../config"
import amqplib from 'amqplib'

class QueueServiceImpl {

  private channel?: amqplib.Channel = undefined
  private onChannelOpenedCallbackArr: (() => void)[] = []

  constructor() {
    amqplib
      .connect(this.connectionString)
      .then(conn => conn.createChannel())
      .then(channel => {
        this.channel = channel
        this.onChannelOpenedCallbackArr
          .forEach(callback => callback())
      })
  }

  private get connectionString() {
    return `amqp://${Config.rabbitmqUsername}:${Config.rabbitmqPassword}@${Config.rabbitmqHost}:${Config.rabbitmqPort}`
  }

  onChannelOpened(callback: () => void) {
    if (this.channel) {
      callback()
    }
    this.onChannelOpenedCallbackArr.push(callback)
  }

  async onItem(queue: string, callback: (item: string) => Promise<void>) {
    if (this.channel) {
      await this.channel.assertQueue(queue)
      this.channel.consume(queue, (msg) => {
        if (msg) {
          callback(msg.content.toString())
            .then(() => this.channel?.ack(msg))
        }
      })
    } else {
      throw new Error("Channel not opened")
    }
  }

  async addItem(queue: string, item: string) {
    if (this.channel) {
      await this.channel.assertQueue(queue)
      this.channel.sendToQueue(queue, Buffer.from(item))
    } else {
      throw new Error("Channel not opened")
    }
  }
}

export const QueueService = new QueueServiceImpl()
