console.info('### Starting bot ###')

import { QueueService } from "./services/queue.service"
import { StockService } from "./services/stock.service"

interface ISocketMessage {
  createdAt: string
  room: string
  text: string
  botArgument: string
  updatedAt: string
  user: string
  _id: string
}

const onNewMessage = async (message: ISocketMessage) => {
  const stockQuote = await StockService.getByStockQuote(message.botArgument)
  await QueueService.addItem('bot-messages', JSON.stringify({
    user: 'stock-bot',
    room: message.room,
    text: stockQuote
      ? `${message.botArgument.toUpperCase()} quote is $${stockQuote} per share`
      : `${message.botArgument.toUpperCase()} is not valid`,
  }))
}

QueueService.onChannelOpened(() => {
  console.info('### Message bot started ###')

  QueueService.onItem('stock-bot-messages', async (item) => {
    console.info('### New message ###')
    await onNewMessage(JSON.parse(item))
  })
})
