import dotenv from 'dotenv'

const environment = process.env.NODE_ENV || 'development'

if (environment === 'development' || environment === 'test') {
  dotenv.config({
    path: require('path').resolve(process.cwd(), `.env`)
  })
}

export const Config = {
  environment,
  mongodbUsername: process.env.MONGO_USERNAME,
  mongodbPassword: process.env.MONGO_PASSWORD,
  mongodbHost: process.env.MONGO_HOST,
  mongodbPort: Number(process.env.MONGO_PORT || 27017),
  rabbitmqUsername: process.env.RABBITMQ_USERNAME,
  rabbitmqPassword: process.env.RABBITMQ_PASSWORD,
  rabbitmqHost: process.env.RABBITMQ_HOST,
  rabbitmqPort: process.env.RABBITMQ_PORT,
  JWTSecret: process.env.JWT_SECRET,
  httpPort: Number(process.env.PORT || 8000),
}
