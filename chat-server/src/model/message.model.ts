import { createSchema, ExtractDoc, Type, typedModel } from 'ts-mongoose'

const MessageSchema = createSchema(
  {
    user: Type.string({ required: true }),
    text: Type.string({ required: true }),
    room: Type.string({ required: true }),
    botName: Type.string({ required: false }),
    botArgument: Type.string({ required: false }),
  }, {
    timestamps: { createdAt: true, updatedAt: true }
  }
);

MessageSchema.set('toJSON', {
  transform: (_1: any, ret: any) => {
    delete ret.password
    return ret
  }
})

export const Message = typedModel('Message', MessageSchema)
export type MessageDoc = ExtractDoc<typeof MessageSchema>
