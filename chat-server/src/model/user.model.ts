import { createSchema, Type, typedModel } from 'ts-mongoose'

const UserSchema = createSchema(
  {
    username: Type.string({ required: true }),
    password: Type.string({ required: true }),
    token: Type.string({ required: false }),
  }, {
    timestamps: { createdAt: true, updatedAt: true }
  }
);

UserSchema.set('toJSON', {
  transform: (_1: any, ret: any) => {
    delete ret.password
    return ret
  }
})

export const User = typedModel('User', UserSchema)
