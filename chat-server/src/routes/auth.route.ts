import * as express from 'express';
import { AuthService } from '../services/auth.service';

export const AuthRoute = express.Router()

interface SignInReqBody {
  username: string
  password: string
}

interface SignUpReqBody {
  username: string
  password: string
}

AuthRoute.post<any, any, SignInReqBody>('/signin', async (req, res) => {
  if (req.body.username && req.body.password) {
    const user = await AuthService.signIn(req.body.username, req.body.password)
    if (user) {
      res.json(user.toJSON())
    } else {
      res.status(401).json({
        code: 401,
        message: 'Username and password is incorrect'
      })
    }
  } else {
    res.status(400).json({
      code: 400,
      message: 'Request body is incorrect'
    })
  }
})

AuthRoute.post<any, any, SignUpReqBody>('/signup', async (req, res) => {
  if (req.body.username && req.body.password) {
    try {
      const user = await AuthService.signUp(req.body.username, req.body.password)
      res.json(user.toJSON())
    } catch {
      res.status(500).json({
        code: 500,
        message: 'Failed to signup'
      })
    }
  } else {
    res.status(400).json({
      code: 400,
      message: 'Request body is incorrect'
    })
  }
})
