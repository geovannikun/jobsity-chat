import * as express from 'express';
import { MessageService } from '../services/message.service';

export const MessageRoute = express.Router()

interface GetMessagesQueryString {
  limit: string
}

MessageRoute.get<any, any, undefined, GetMessagesQueryString>('/', async (req, res) => {
  if (req.query.limit) {
    res.send({
      messages: await MessageService.getLastMessages(Number(req.query.limit))
    })
  } else {
    res.status(400).json({
      code: 400,
      message: 'Request params are incorrect'
    })
  }
})
