
import http from 'http'
import { Server } from "socket.io"
import { Message, MessageDoc } from './model/message.model'
import { AuthService } from './services/auth.service'
import { MessageService } from './services/message.service'
import { QueueService } from './services/queue.service'

class SocketServerImpl {
  private io?: Server

  start(httpServer: http.Server) {
    this.io = new Server(httpServer, {
      cors: { origin: '*' },
    })

    this.io.use(async (socket, next) => {
      // @ts-ignore
      const token = socket.handshake.auth.token
      const user = await AuthService.getUserByAuthToken(token)
      if (!user) {
        const err = new Error("Not authorized")
        next(err)
      } else {
        // @ts-ignore
        socket.user = user
        next()
      }
    })

    this.io.on('connection', (socket) => {
      socket.onAny(async (eventName: string, text: string) => {
        if (eventName.includes('_messages')) {
          const [room, ] = eventName.split('_')
          const message = await MessageService.createMessage(
            room,
            socket.user.username,
            text,
          )
          this.emitMessage(message)
        }
      })
    })
  }

  async emitMessage(message: MessageDoc) {
    if (this.io) {
      if (message.botName && message.botArgument) {
        await QueueService.addItem(`${message.botName}-bot-messages`, JSON.stringify(message.toJSON()))
      }
      this.io.emit(`${message.room}_messages`, message.toJSON())
    } else {
      throw new Error('Socket not started')
    }
  }
}

export const SocketServer = new SocketServerImpl()
