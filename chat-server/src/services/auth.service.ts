import { User } from '../model/user.model'
import { HashService } from './hash.service'
import { TokenService } from './token.service'

class AuthServiceImpl {

  async getUserByAuthToken(authToken: string) {
    return await User.findOne({ token: authToken })
  }

  async signIn(username: string, password: string) {
    const user = await User.findOne({ username })
    if (user && await HashService.checkHash(password, user.password)) {
      user.token = undefined
      user.token = await TokenService.createToken(JSON.stringify(user.toJSON()))
      await user.save()
      return user
    }

    return undefined
  }

  async signUp(username: string, password: string) {
    const user = await User.findOne({ username })
    if (!user) {
      return await User.create({
        username,
        password: await HashService.createHash(password),
      })
    }

    throw new Error('User already registered!')
  }
}

export const AuthService = new AuthServiceImpl()
