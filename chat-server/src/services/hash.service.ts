import * as bcrypt from 'bcrypt'

class HashServiceImpl {

  private saltRounds = 10

  async createHash(plainString: string) {
    return await bcrypt.hash(plainString, this.saltRounds)
  }

  async checkHash(plainString: string, hashedString: string) {
    return await bcrypt.compare(plainString, hashedString);
  }
}

export const HashService = new HashServiceImpl()
