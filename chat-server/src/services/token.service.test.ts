import { TokenService } from './token.service'

describe('TokenService', () => {
  const user = "{ username: 'test' }"

  it('generate token should work', async () => {
    const token = await TokenService.createToken(user)
    expect(token).toBeDefined();
  })

  it('generate hash and validate with same password should work', async () => {
    const token = await TokenService.createToken(user)
    expect(await TokenService.isTokenValid(token)).toBe(true);
  })

  it('validate hash with any non hash string should not work', async () => {
    expect(await TokenService.isTokenValid(user)).toBe(false);
  })
})
