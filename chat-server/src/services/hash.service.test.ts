import { HashService } from './hash.service'

describe('AuthService', () => {
  const password = 'password'
  const password2 = 'password2'

  it('generate hash should work', async () => {
    const hash = await HashService.createHash(password)
    expect(hash).toBeDefined();
  })

  it('generate hash and validate with same password should work', async () => {
    const hash = await HashService.createHash(password)
    expect(await HashService.checkHash(password, hash)).toBe(true);
  })

  it('generate hash and validate with other password should not work', async () => {
    const hash = await HashService.createHash(password)
    expect(await HashService.checkHash(password2, hash)).toBe(false);
  })
})
