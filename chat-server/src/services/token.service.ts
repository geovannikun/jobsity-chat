import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import { Config } from '../config'

class TokenServiceImpl {
  async createToken(content: string) {
    if (Config.JWTSecret) {
      return jwt.sign({
        content: `${content}:${crypto.randomBytes(16).toString('hex')}`,
        iat: Math.floor(Date.now() / 1000) - 30,
      }, Config.JWTSecret)
    } else {
      throw new Error('JWT Secret not configured')
    }
  }

  async isTokenValid(jws: string) {
    if (Config.JWTSecret) {
      try {
        jwt.verify(jws, Config.JWTSecret)
        return true
      } catch {
        return false
      }
    } else {
      throw new Error('JWT Secret not configured')
    }
  }
}

export const TokenService = new TokenServiceImpl()
