import { Message } from '../model/message.model';

class MessageServiceImpl {

  private botRegex = /^\/(.+)[=](.+)$/

  async createMessage(room: string, username: string, text: string) {
    const textTrimmed = text.trim()
    return await Message.create({
      user: username,
      room,
      text: textTrimmed,
      ...(
        this.isBotMessage(textTrimmed)
          ? this.getBotFromMessage(textTrimmed)
          : {}
      )
    })
  }

  isBotMessage(text: string) {
    return this.botRegex.test(text.trim())
  }

  async getLastMessages(count: number) {
    return await Message.find()
      .limit(count)
      .sort('-createdAt')
  }

  getBotFromMessage(text: string) {
    const [, botName, botArgument] = text.split(this.botRegex)
    return {
      botName,
      botArgument,
    }
  }
}

export const MessageService = new MessageServiceImpl()
