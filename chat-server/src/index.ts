import express from 'express'
import { createServer } from 'http'
import mongoose from 'mongoose'
import helmet from 'helmet'
import cors from 'cors'
import bodyParser from 'body-parser'
import { AuthRoute } from './routes/auth.route'
import { Config } from './config'
import { QueueService } from './services/queue.service'
import { SocketServer } from './socket.server'
import { MessageService } from './services/message.service'
import { MessageRoute } from './routes/message.route'

const app = express()
const httpServer = createServer(app)

QueueService.onChannelOpened(() => {
  SocketServer.start(httpServer)
  QueueService.onItem('bot-messages', async (item) => {
    const botMessage = JSON.parse(item)
    const message = await MessageService.createMessage(
      botMessage.room,
      botMessage.user,
      botMessage.text,
    )
    SocketServer.emitMessage(message)
  })
})

app.use(helmet())
app.use(cors())
app.use(bodyParser.json())
app.use(express.json())
app.use('/auth', AuthRoute)
app.use('/message', MessageRoute)

app.use((req, res, next) => {
  res.status(404).send({
    code: 404,
    message: 'Not found'
  })
})

if (Config.mongodbUsername && Config.mongodbPassword) {
  mongoose
    .connect(`mongodb://${Config.mongodbHost}:${Config.mongodbPort}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      auth: {
        user: Config.mongodbUsername,
        password: Config.mongodbPassword,
      }
    })
    .then(() => {
      httpServer.listen(Config.httpPort, () => {
        console.info(`⚡️[server]: Server is running at http://localhost:${Config.httpPort}`)
      })
    })
} else {
  console.error('Mongo auth not configured')
}
