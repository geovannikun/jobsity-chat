import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import './App.css';

const ENDPOINT = `http://${process.env.REACT_APP_CHAT_SERVER_HOST}:${process.env.REACT_APP_CHAT_SERVER_PORT}`;

export const SignInPage = () => {
  let history = useHistory()

  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [error, setError] = React.useState<string>()

  const onChangeUsername = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value)
    setError(undefined)
  }, [])

  const onChangePassword = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
    setError(undefined)
  }, [])

  const login = React.useCallback(async () => {
    const response = await fetch(`${ENDPOINT}/auth/signin`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password
      })
    }).then(r => r.json())

    if (response.code >= 400) {
      setError(response.message)
    } else {
      localStorage.setItem('token', response.token)
      history.push('/')
    }
  }, [username, password, history])

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Sign In
        </p>
        <input placeholder={'Username'} value={username} onChange={onChangeUsername}/>
        <input placeholder={'Password'} type={'password'} value={password} onChange={onChangePassword}/>
        {(!!error) && (
          <p>{error}</p>
        )}
        <button onClick={login}>Login</button>

        <Link to="/signup">Sign Up</Link>
      </header>
    </div>
  );
}
