import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import './App.css';

const ENDPOINT = `http://${process.env.REACT_APP_CHAT_SERVER_HOST}:${process.env.REACT_APP_CHAT_SERVER_PORT}`;

export const SignUpPage = () => {
  let history = useHistory()

  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [error, setError] = React.useState<string>()

  const onChangeUsername = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value)
    setError(undefined)
  }, [])

  const onChangePassword = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
    setError(undefined)
  }, [])

  const signup = React.useCallback(async () => {
    const response = await fetch(`${ENDPOINT}/auth/signup`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password
      })
    }).then(r => r.json())

    if (response.code >= 400) {
      setError(response.message)
    } else {
      history.push('/signin')
    }
  }, [username, password, history])

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Sign Up
        </p>
        <input placeholder={'Username'} value={username} onChange={onChangeUsername}/>
        <input placeholder={'Password'} type={'password'} value={password} onChange={onChangePassword}/>
        {(!!error) && (
          <p>{error}</p>
        )}
        <button onClick={signup}>Create</button>

        <Link to="/signin">Sign In</Link>
      </header>
    </div>
  );
}
