import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import socketIOClient from "socket.io-client";
import './ChatPage.css';

const ENDPOINT = `http://${process.env.REACT_APP_CHAT_SERVER_HOST}:${process.env.REACT_APP_CHAT_SERVER_PORT}`;

interface ISocketMessage {
  createdAt: string
  room: string
  text: string
  updatedAt: string
  user: string
  _id: string
}

export const ChatPage = () => {
  let history = useHistory()
  // @ts-ignore
  let { chatName } = useParams();

  const socketRoom = React.useMemo(
    () => `${chatName}_messages`,
    [chatName]
  )

  const [messages, setMessages] = React.useState<ISocketMessage[]>([])
  const [messageToSend, setMessageToSend] = React.useState('')
  const [socket, setSocket] = React.useState<socketIOClient.Socket>()

  const onChangeMessageToSend = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setMessageToSend(event.target.value)
  }, [])

  const sendMessage = React.useCallback(() => {
    socket?.emit(socketRoom, messageToSend)
  }, [socket, socketRoom, messageToSend])

  React.useEffect(() => {
    if (!localStorage.getItem('token')) {
      history.push('/signin')
    }
  }, [history])

  React.useEffect(() => {
    var messageSection = document.querySelector(".chat-page-messages-section");
    if (messageSection) {
      messageSection.scrollTop = messageSection.scrollHeight
    }
  }, [messages])

  React.useEffect(() => {
    fetch(`${ENDPOINT}/message?limit=50`)
      .then(res => res.json())
      .then(res => {
        setMessages((res.messages as ISocketMessage[]).sort((a, b) => {
          if (a.createdAt > b.createdAt) {
            return 1
          } else if (a.createdAt < b.createdAt) {
            return -1
          }
          return 0
        }))
      })
  }, [])

  React.useEffect(() => {
    if (socketRoom) {
      const socket_io = socketIOClient.io(ENDPOINT, {
        auth: {
          token: localStorage.getItem('token')
        }
      });
      socket_io.connect()
      setSocket(socket_io)
      socket_io.on(socketRoom, (message: ISocketMessage) => {
        setMessages(msgs => ([
          ...msgs,
          message
        ]))
        setMessageToSend('')
      })

      return () => { socket_io.disconnect() };
    }
  }, [socketRoom]);

  return (
    <div className="chat-page">
      <header>
        Chat name: {chatName}
      </header>
      <section className="chat-page-messages-section">
        <ul>
          {messages.map(m => (
            <li key={m._id} className="chat-page-message">
              <small>{m.user}</small><br/>
              <small>{m.createdAt}</small><br/>
              {m.text}
            </li>
          ))}
        </ul>
      </section>
      <section className="chat-page-input-section">
        <input placeholder={'Message'} value={messageToSend} onChange={onChangeMessageToSend}/>
        <button onClick={sendMessage}>Send</button>
      </section>
    </div>
  );
}
