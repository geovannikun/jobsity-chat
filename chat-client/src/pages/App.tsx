import React from 'react';
import { useHistory } from 'react-router-dom';
import './App.css';

export const App = () => {
  let history = useHistory()

  const [chatName, setChatName] = React.useState('')

  React.useEffect(() => {
    if (!localStorage.getItem('token')) {
      history.push('/signin')
    }
  }, [history])

  const onChangeChatName = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setChatName(event.target.value)
  }, [])

  const goToChat = React.useCallback(() => {
    history.push(`/chat/${chatName}`)
  }, [history, chatName])

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Chat name
        </p>
        <input placeholder={'Place the chat name here'} value={chatName} onChange={onChangeChatName}/>
        <button onClick={goToChat}>Go to chat</button>
      </header>
    </div>
  );
}
